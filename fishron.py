import os
import sys
import subprocess
import signal
import janitor
import threading
import time

if __name__ == "__main__":
    # Read the required environment variables
    fishron_dir = os.environ['FISHRON_DIR']
    servers_dir = os.environ['FISHRON_SERVERS_DIR']
    server_version = os.environ['FISHRON_SERVER_VERSION']
    server_config = os.environ['FISHRON_SERVER_CONFIG']
    drive_secret = os.environ['FISHRON_GDRIVE_SECRET']
    terraria_worlds_folder = os.environ['FISHRON_TERRARIA_WORLDS_FOLDER']

    terraria_server_command = f"mono --server --gc=sgen -O=all {servers_dir}/{server_version}/Linux/TerrariaServer.exe -config {server_config}"
    
    print(f"[Fishron] Launching terraria server: {terraria_server_command}")
    # The terraria server does something weird with stdout: we have to flush in order
    # to see the previous command
    sys.stdout.flush()
    server_process = subprocess.Popen(terraria_server_command.split(), stdin=subprocess.PIPE, text=True, bufsize=0)
    print(f"[Fishron] Server process PID is {server_process.pid}")

    print("[Fishron] Launching janitor")
    janitor_thread = threading.Thread(target = lambda: janitor.run(drive_secret, server_version, terraria_worlds_folder), daemon=True)
    janitor_thread.start()

    def sigterm_handler(signal, frame):
        print("[Fishron] Received SIGTERM, notifying terraria server to save and exit")

        server_process.stdin.write('exit\n')
        server_process.stdin.flush()
        server_process.wait()

    signal.signal(signal.SIGTERM, sigterm_handler)

    while True:
        sys.stdout.flush()
        time.sleep(1)

