import requests
import time
from pydrive2.auth import GoogleAuth
from pydrive2.drive import GoogleDrive
from oauth2client.service_account import ServiceAccountCredentials
import tempfile, shutil
import os
import datetime
import argparse
import schedule

# TODO No, these aren't secrets: but the fact that these are hardcoded does mean that
# this script is useless to anyone but me at the moment :(
PARENT_FOLDER_ID = '1ob4HVQkCHCjRleXjWPDeM0NOKOZSoa6V'
STATUS_FILE_ID = "1TjzYA3aaFJMWNhITyWzlZGndF1aOOIUYLns8PiQfrIo"
BACKUP_FOLDER_ID = "1i3Aei0zzNxJCAJerdCib7V3LyomuZjOY"

def get_drive(client_secrets_path):
    scope = ["https://www.googleapis.com/auth/drive"]
    gauth = GoogleAuth()
    gauth.auth_method = 'service'
    gauth.credentials = ServiceAccountCredentials.from_json_keyfile_name(client_secrets_path, scope)
    return GoogleDrive(gauth)

def get_ip():
    return requests.get('https://api.ipify.org').content.decode('utf8')

def get_time_str():
    return datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d_%H%M%S_%Z")

# def do_update(update_ip: bool, backup_worlds: bool, last_world_backup_timestamp: str):
def get_update_txt(current_ip: str, terraria_version: str, backup_hreadable: str, current_time: str):
    return f"""--- Last Updated {current_time} ---
Server IP: {current_ip}
Server Terraria Version: {terraria_version}
Last Backup Time: {backup_hreadable}
    """

def backup_worlds_to_drive(drive, local_folder_path, backup_name):

    with tempfile.TemporaryDirectory() as tmpdir:
        path = os.path.join(tmpdir, backup_name)
        shutil.make_archive(path, 'zip', local_folder_path)

        drive_file = drive.CreateFile({'parents': [{'id': BACKUP_FOLDER_ID}],  'title': backup_name + '.zip'})
        drive_file.SetContentFile(path + '.zip')
        drive_file.Upload()

class UpdateInfo:
    def __init__(self,):
        self.ip = "Unknown"
        self.last_backup: str = "Never"

def jprint(string: str):
    print("[janitor] " + string)

def run(drive_secret: str, terraria_version: str, terraria_worlds_folder: str):
    status = UpdateInfo()

    def RefreshServerIp():
        updated_ip = get_ip()
        if updated_ip != status.ip:
            jprint(f"Updating server IP from {status.ip} to {updated_ip}")
            status.ip = updated_ip

    def BackupWorlds():
        drive = get_drive(drive_secret)
        backup_worlds_to_drive(drive, terraria_worlds_folder, get_time_str())
        status.last_backup = get_time_str()
        jprint(f"Backed up worlds at {status.last_backup}")

    def UploadInfoFile():
        drive = get_drive(drive_secret)
        update_file = drive.CreateFile({'parents': [{'id': PARENT_FOLDER_ID}],  'title': 'Terraria-Server.info', 'id': STATUS_FILE_ID})
        update_file.SetContentString(get_update_txt(status.ip, terraria_version, status.last_backup, get_time_str()))
        update_file.Upload()
        jprint("Refreshed info file")

    jprint("Starting fishron janitor")
    schedule.every(5).minutes.do(RefreshServerIp)
    schedule.every(1).days.do(BackupWorlds)
    schedule.every(5).minutes.do(UploadInfoFile)

    # The .every() calls above don't schedule the job for *now*
    schedule.run_all()
    while True:
        schedule.run_pending()
        time.sleep(1)

