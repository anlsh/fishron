Fishron
=========
Runs a terraria server and performs periodic backups to google drive

TODO: None of the google drive stuff will work for anyone but me! The IDs are hardcoded

Setup
=====

1. Clone this repository to some directory: that becomes `FISHRON_DIR`
2. Download the terraria server to some directory, eg `/some/prefix/xxxx`
* `/some/prefix` becomes `FISHRON_SERVERS_DIR`
* `xxxx` becomes `FISHRON_SERVER_VERSION`
3. Download a client secrets file for some google cloud service account, and populate `FISHRON_GDRIVE_SECRET` with its path
4. Create a `.config` file for the terraria server: its path becomes `FISHRON_SERVER_CONFIG`
5. Set `FISHRON_TERRARIA_WORLDS_FOLDER` to whatever folder should be backed up to google drive
6. Create a python3 virtualenv, install `requirements.txt`: the venv's path becomes `FISHRON_VENV`
7. Edit the `.service` file to set the environment variables
8. Install/enable/start the systemd service (might need to chmod 664 it)


Things I had to look up
==================
1. The server is only compiled for x86_64: but it's pretty easy to get it running on arm actually (SEE BELOW)

   See this reddit URL for instructions on running arm64
https://www.reddit.com/r/Terraria/comments/gl5fl8/guide_how_to_setup_a_terraria_14_server_on_a/

2. Fishron depends on GNU tail: see https://stackoverflow.com/questions/1058047/wait-for-a-process-to-finish/41613532#41613532and its usage in the ExecStop of the service file

